import { fighterData, IFighter } from '../helpers/apiHelper';

interface IFighterService
{
    getFighters(): Promise<IFighter[] | IFighter>,
    getFighterDetails(id: string): Promise<IFighter[] | IFighter>;
}

class FighterService implements IFighterService
{
    public getFighters()
    {
        return fighterData('fighters.json');
    }

    public getFighterDetails(id)
    {
        return fighterData(`details/fighter/${id}.json`);
    }
}

const fighterService = new FighterService();

export { fighterService, IFighterService }